# Pacotes a serem utilizados
library(tidyverse)
library(ggplot2)


### AJUSTANDO O BANCO DE DADOS ###

# Dividindo os dados por frequência da variável de interesse
df_ana5 = df_ps %>%
  group_by(Department) %>%
  summarise(freq=n())

# Substitui os valores da variável "Unidade de Negócios" para traduzi-los pro português
df_ana5[df_ana5 == 'Accounting'] <- 'Contabilidade'
df_ana5[df_ana5 == 'Engineering'] <- 'Engenharia'
df_ana5[df_ana5 == 'Finance'] <- 'Finanças'
df_ana5[df_ana5 == 'Human Resources'] <- 'RH'
df_ana5[df_ana5 == 'IT'] <- 'TI'
df_ana5[df_ana5 == 'Sales'] <- 'Vendas'

# Observando o dataframe
df_ana5


### GRÁFICO ###

# Cria coluna extra que será utilizada para detalhar cada barra do gráfico
df_ana5$label_gra = 
  str_replace(paste(df_ana5$freq, ' (', round(df_ana5$freq/11, 2), '%)', sep=''),'\\.', ',')

# Gera gráfico
ggplot(df_ana5) +
  aes(x = fct_reorder(Department, freq, .desc=T), y = freq, label = label_gra) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, #hjust = .5,
    size = 3
  ) + 
  labs(x = "Departamento", y = "Nº de funcionários(as)") +
  scale_y_continuous(breaks=seq(0, 300, 50)) +
  theme_estat()
ggsave("figura_4.png", width = 158, height = 93, units = "mm")

