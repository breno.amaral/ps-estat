# Pacotes a serem utilizados
library(tidyverse)
library(ggplot2)

# Ajustando dataframe - agrupando por frequência
df_ana6 <- df_ps %>%
  group_by(City) %>% dplyr::summarise(freq = n())
# Ordenando por frequência
df_ana6 <- df_ana6[order(df_ana6$freq, decreasing = TRUE), ]
# Mantendo apenas os 5 primeiros
df_ana6 <- df_ana6[1:5, ]

# Cria coluna extra que será utilizada para detalhar cada barra do gráfico
df_ana6$label_gra = 
  str_replace(paste(df_ana6$freq, ' (', round(df_ana6$freq*100/sum(df_ana6$freq), 2), '%)',
                    sep=''),'\\.', ',')

# Total das 5 cidades mais frequentes 
sum(df_ana6$freq)

# Gera gráfico
ggplot(df_ana6) +
  aes(x = fct_reorder(City, freq, .desc=T), y = freq, label = label_gra) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, #hjust = .5,
    size = 3
  ) + 
  labs(x = "Cidade", y = "Nº de funcionários(as)") +
  scale_y_continuous(breaks=seq(0, 140, 20)) +
  theme_estat()
ggsave("figura_6.png", width = 158, height = 93, units = "mm")
